import './App.css';
import LearnMap1 from './practicecomponent/LearnMap1';

import Address from './practicecomponent/Address';
import Age from './practicecomponent/Age';
import College from './practicecomponent/College';
import Detail1 from './practicecomponent/Detail1';
import Details from './practicecomponent/Details';
import EffectOnDifferentData from './practicecomponent/EffectOnDifferentData';
import Info from './practicecomponent/Info';
import LearnTernary from './practicecomponent/LearnTernary';
import Location from './practicecomponent/Location';
import Marks from './practicecomponent/Marks';

import Name from './practicecomponent/Name';
import Restriction from './practicecomponent/Restriction';
import LearnMap2 from './practicecomponent/LearnMap2';
import ButtonClick from './practicecomponent/ButtonClick';
import LearnUseStateHook from './practicecomponent/LearnUseStateHook/LearnUseStateHook';
import LearnState2 from './practicecomponent/LearnUseStateHook/LearnState2';
import UseStateImage from './practicecomponent/LearnUseStateHook/UseStateImage';
import Names from './practicecomponent/LearnUseStateHook/Names';
import AppComponent from './practicecomponent/LearnUseStateHook/AppComponent';
import IncrementByChoice from './practicecomponent/LearnUseStateHook/IncrementByChoice';
import { createContext, useContext, useState } from 'react';
import Toggle from './practicecomponent/LearnUseStateHook/Toggle';
import WhyUseState from './practicecomponent/LearnUseStateHook/WhyUseState';
import Increment from './practicecomponent/LearnUseStateHook/Increment';
import LearnUseEffectHook from './practicecomponent/LearnUseEffectHook/LearnUseEffectHook';
import LearnCleanUpFunction from './practicecomponent/LearnUseEffectHook/LearnCleanUpFunction';
import MyLinks from './practicecomponent/MyLinks';
import MyRoutes from './practicecomponent/MyRoutes';
import Learn1UseEffectHook from './practicecomponent/LearnUseRefHook/Learn1UseEffectHook';
import AddToLocalStorage from './practicecomponent/LearnLocalStorage/AddToLocalStorage';
import GetLocalStorageData from './practicecomponent/LearnLocalStorage/GetLocalStorageData';
import RemoveLocalStorageData from './practicecomponent/LearnLocalStorage/RemoveLocalStorageData';
import AddDataToSessionStorage from './practicecomponent/LearnSessionStorage/AddDataToSessionStorage';
import GetDataOfSessionStorage from './practicecomponent/LearnSessionStorage/GetDataOfSessionStorage';
import RemoveDataFromSessionStorage from './practicecomponent/LearnSessionStorage/RemoveDataFromSessionStorage';
import { Formik } from 'formik';
import FormikForm from './practicecomponent/LearnFormik/FormikForm';
import FormikTutorial from './practicecomponent/LearnFormik/FormikTutorial';
import Parent from './practicecomponent/LearnPropDrilling/Parent';

export let context1 =createContext()
export let context2 =createContext()


function App() {

  let a = <p> this is paragraph</p>;
  let  [name,setName] = useState("san")
  let [age,setAge] = useState(22)
  let [address,setAddress] = useState("sifal")

 // let [showName, setShowName] = useState(false)
  // let [showCleanup, setShowCleanup] = useState(false)


  let b =`${1 + 1}`;//`2`
  return (
    <div> 

     {/* <Name></Name>
      <Age></Age>
      <Address></Address> 
      <Details Name="sanit" Address="ktm" Age ={99}></Details>
      <Detail1 Name="rust" Address="All" Age ={100} ></Detail1>
      <College Name="Enlightn" Address="Everewere"  ></College>

     
     
     
      <h1 style={{color:"blue",backgroundColor:"blueviolet"}}>this is header </h1>
      <p style={{color:"green",backgroundColor:"red"}}> this is paragraph</p>
      <span >this is span</span>
      <span className='success'>this is another span</span>

      {a}
      {name}
      {age}

      <a href="https://facebook.com/" target=" "> facebook </a >
      <br></br>
      <img src= "./goldenapple.png"></img> 
        <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <h1 className='success'>Sussessful </h1>
      <p className='error'> Error!!!!</p>
      <span className='info'>your information</span>
      <span className='warning'>whattt warning warning</span>

      {console.log(age)}   */}

      {/* <LearnTernary> */}

     {/* </LearnTernary>
     <Marks></Marks>
     <EffectOnDifferentData></EffectOnDifferentData>
     <Restriction></Restriction>
     <Location country="nepal" province="bagmati" district="kathmandu" exactlocation="sifal" age ={29}></Location>
      <Info 
      // name = "san"age={99} fatherDetail={{fname:"dan" fage:"55"}}

      ></Info>  */}

      {/* <LearnMap1></LearnMap1>
      <LearnMap2></LearnMap2>
      <ButtonClick></ButtonClick>
      <LearnUseStateHook></LearnUseStateHook>
      <LearnState2></LearnState2> */}
      {/* <UseStateImage></UseStateImage>

      {/* {
        showName? <Names></Names>:null
      }
     

      <button onClick = {()=>{
        setShowName(true)
      }}>Show Name</button>
      <button
      onClick = {()=>{
        setShowName(false)
      }}
      >hide Name</button> */}

      {/* <AppComponent></AppComponent>
      <IncrementByChoice></IncrementByChoice>
      <Toggle></Toggle>
      <WhyUseState></WhyUseState>
      <Increment></Increment>
      <LearnUseEffectHook></LearnUseEffectHook> */}
      
       {/* {
        showCleanup? <LearnCleanUpFunction></LearnCleanUpFunction>:null
      }
     

      <button onClick = {()=>{
        setShowCleanup(true)
      }}>Show </button><br></br>
      <button
      onClick = {()=>{
        setShowCleanup(false)
      }}
      >hide </button>  */}
      {/* <LearnCleanUpFunction></LearnCleanUpFunction> */}
      {/* <MyLinks></MyLinks>
      <MyRoutes></MyRoutes> */}
      {/* <Learn1UseEffectHook></Learn1UseEffectHook>
      {/* <AddToLocalStorage></AddToLocalStorage> */}
      {/* <GetLocalStorageData></GetLocalStorageData> */}
      {/* <RemoveLocalStorageData></RemoveLocalStorageData> */}
      {/* <AddDataToSessionStorage></AddDataToSessionStorage> */}
      {/* <GetDataOfSessionStorage></GetDataOfSessionStorage> */}
      {/* <RemoveDataFromSessionStorage></RemoveDataFromSessionStorage> */}
      {/* <FormikForm></FormikForm> */}
      {/* <FormikTutorial></FormikTutorial> */}
         <context1.Provider value = {{name:name,age:age,setName,setAge}}>
          <context2.Provider value={{address:address,setAddress:setAddress}}>
         <Parent></Parent>
         </context2.Provider>
         </context1.Provider>
       
      
      

    
      

      
       </div>
  );
}

export default App;
