export let days = [
    {
      label: "Sunday",
      value: "day1",
    },
    {
      label: "Monday",
      value: "day2",
    },
    {
      label: "Tuesday",
      value: "day3",
    },
    {
      label: "Wednesday",
      value: "day4",
    },
    {
      label: "Thursday",
      value: "day5",
    },
    {
      label: "Friday",
      value: "day6",
    },
    {
      label: "Saturday",
      value: "day7",
    },
  ];

export let genders = [
    {label:"Male", value:"male"},
    {label:"Female", value:"female"},
    {label:"Other", value:"other"}]
