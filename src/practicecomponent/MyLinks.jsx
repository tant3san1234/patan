import React from 'react'
import { NavLink } from 'react-router-dom'

const MyLinks = () => {
  return (
    <div> 
         <NavLink to = "/products/create" style={{marginRight:"20px"}}>create product</NavLink>
         <NavLink to = "/products" style={{marginRight:"20px"}}>product</NavLink>
         <NavLink to = "/students/create" style={{marginRight:"20px"}}>create student</NavLink>
         <NavLink to = "/students" style={{marginRight:"20px"}}>student</NavLink>
         <NavLink to = "/admin/register" style={{marginRight:"20px"}}>Admin Register</NavLink>
         <NavLink to = "/admin/login" style={{marginRight:"20px"}}>Admin Login</NavLink>
         <NavLink to = "/admin/my-profile" style={{marginRight:"20px"}}>My Profile</NavLink>
         {/* <NavLink to = "/admin/profile-update" style={{marginRight:"20px"}}>My Profile</NavLink> */}
         <NavLink to = "/admin/logout" style={{marginRight:"20px"}}> logout</NavLink>
         <NavLink to = "/admin/update-password" style={{marginRight:"20px"}}> Update Password</NavLink>
         <NavLink to = "/admin/read-all-user" style={{marginRight:"20px"}}> Read All User</NavLink>
    </div>
  )
}

export default MyLinks

/* 
""

*/