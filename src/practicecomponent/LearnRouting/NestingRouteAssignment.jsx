import React from "react";
import { Navigate, Outlet, Route, Routes } from "react-router-dom";
import ReadAllProduct from "../product/ReadAllProduct";
import ReadSpecificProduct from "../product/ReadSpecificProduct";
import UpdateProduct from "../product/UpdateProduct";
import UpdateSpecificProduct from "../product/UpdateSpecificProduct";
// import { ReadAllStudent } from '../student/ReadAllStudent'
import ReadSpecificStudent from "../student/ReadSpecificStudent";
import CreateStudent from "../student/CreateStudent";
import UpdateStudent from "../student/UpdateStudent";
import MyLinks from "../MyLinks";
import CreateProduct from "../product/CreateProduct";
import ReadAllStudent from "../student/ReadAllStudent";
import AdminRegister from "../webuser/AdminRegister";
import AdminVerify from "../webuser/AdminVerify";
import AdminLogin from "../webuser/AdminLogin";
import AdminLogout from "../webuser/AdminLogout";
import AdminProfile from "../webuser/AdminProfile";
import AdminProfileUpdate from "../webuser/AdminProfileUpdate";
import AdminUpdatePassword from "../webuser/AdminUpdatePassword";
import AdminForgotPassword from "../webuser/AdminForgotPassword";
import AdminResetPassword from "../webuser/AdminResetPassword";
import AdminReadAllUser from "../webuser/AdminReadAllUser";
import { useSelector } from "react-redux";
import AdminReadSpecificUser from "../webuser/AdminReadSpecificUser";
import AdminUpdateSpecificUser from "../webuser/AdminUpdateSpecificUser";

const NestingRouteAssignment = () => {
 

  return (
    <div>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <MyLinks></MyLinks>
              <Outlet></Outlet>
              {/* <div>This is Footer</div> */}
            </div>
          }
        >
          <Route index element={<div>Home Page</div>}></Route>

          <Route path="*" element={<div>404 Page Not Found</div>}></Route>
          <Route
              path="reset-password"
              element={<AdminResetPassword></AdminResetPassword>}
            ></Route>

          <Route
            path="verify-email"
            element={<AdminVerify></AdminVerify>}
          ></Route>

          {/*Products*/}
          <Route
            path="products"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<ReadAllProduct></ReadAllProduct>}></Route>

            <Route
              path=":id"
              element={<ReadSpecificProduct></ReadSpecificProduct>}
            ></Route>

            <Route
              path="create"
              element={<CreateProduct></CreateProduct>}
            ></Route>

            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route index element={<UpdateProduct></UpdateProduct>}></Route>

              <Route
                path=":id"
                element={<UpdateSpecificProduct></UpdateSpecificProduct>}
              ></Route>
            </Route>
          </Route>

          {/*Students*/}
          <Route
            path="students"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<ReadAllStudent></ReadAllStudent>}></Route>

            <Route
              path=":id"
              element={<ReadSpecificStudent></ReadSpecificStudent>}
            ></Route>

            <Route
              path="create"
              element={<CreateStudent></CreateStudent>}
            ></Route>

            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route
                index
                element={
                  <div>
                    <Navigate to="/*"></Navigate>
                  </div>
                }
              ></Route>

              <Route
                path=":Id"
                element={<UpdateStudent></UpdateStudent>}
              ></Route>
            </Route>
          </Route>
          
          {/* admin */}
          <Route
            path="admin"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<div>Admin dashboard</div>}></Route>
            <Route
              path="register"
              element={<AdminRegister></AdminRegister>}
            ></Route>
            <Route path="login" element={<AdminLogin></AdminLogin>}></Route>
            <Route
              path="verify-email"
              element={<AdminVerify></AdminVerify>}
            ></Route>
            <Route
              path="my-profile"
              element={<AdminProfile></AdminProfile>}
            ></Route>
            <Route path="logout" element={<AdminLogout></AdminLogout>}></Route>
            <Route
              path="update-profile"
              element={<AdminProfileUpdate></AdminProfileUpdate>}
            ></Route>
            <Route
              path="update-password"
              element={<AdminUpdatePassword></AdminUpdatePassword>}
            ></Route>
            <Route
              path="forgot-password"
              element={<AdminForgotPassword></AdminForgotPassword>}
            ></Route>
            <Route
              path="read-all-user"
              element={<AdminReadAllUser></AdminReadAllUser>}
            ></Route>
            <Route
              path=":id"
              element={<AdminReadSpecificUser></AdminReadSpecificUser>}
            ></Route>
            <Route
              path="update"
              element={<div>
                <Outlet></Outlet>
                </div>
                }
            >
              <Route path=":id" element={<AdminUpdateSpecificUser></AdminUpdateSpecificUser>} ></Route>
            </Route>
          </Route>

          

          {/* admin verify  */}

          {/* admin login  */}
        </Route>
      </Routes>
      {/* <div>ram</div> */}
    </div>
  );
};

export default NestingRouteAssignment;
