import { Field } from 'formik'
import React from 'react'

const FormikInput = ({name,label,type,onChange,required,...props}) => {

  return (
    <div>
        <Field name ={name}>
                        {
                         ({field,form,meta})=>{
                          // console.log(meta)
                        //  console.log(field)
                        
                            return(
                              <div>
                               <label htmlFor={name}>
                                {label} {" "}
                               {required?<span style={{color:"red"}}>*</span>:null}
                               </label>
                                  <input 
                                  {...field}
                                  {...props}
                                  type={type}
                                   id={name}
                                    value={meta.value}
                                    onChange={onChange=!!onChange?onChange:field.onChange}
                                    // onChange={field.onChange} // ==> formik.setFieldValue("firstName",e.target.value)
                                //    onChange={(e) => {
                                //       // console.log("changed");
                                //       // setFirstName(e.target.value);
                                //       formik.setFieldValue("firstName",e.target.value) 
                                //       formik.setFieldValue("lastName",brown) //for putting more than one value put this instead of onChange={field.onChange} 
                                //       // console.log(e.target.value);
                                //   }}
                                  
                                  ></input>
                                  { meta.touched && meta.error?(
                                       <div style={{color:"red"}}>{meta.error}</div>):null
                                  }
                              </div>
                            )
                          }
                        }
                      </Field>
    </div>
  )
}

export default FormikInput