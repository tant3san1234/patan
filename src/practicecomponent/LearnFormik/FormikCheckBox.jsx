import { Field } from 'formik'
import React from 'react'

const FormikCheckBox = ({name,label,onChange,required,...props}) =>{

    return (
      <div>
          <Field name ={name}>
                          {
                           ({field,form,meta})=>{
                            // console.log(meta)
                          //  console.log(field)
                          
                              return(
                                 <div>
                                      {/* <label htmlFor="isMarried">Are you Married?</label> */}
                                      <label htmlFor={name}>
                                        {label} {" "}
                                        {required?<span style={{color:"red"}}>*</span>:null}
                                        </label><br></br>
                                        <input 
                                        {...Field}
                                        {...props}
                                            type="checkbox" 
                                            id={name}
                                            onChange={onChange=!!onChange?onChange:field.onChange}
                                            checked={meta.value}
                                            // value={isMarried}
                                            // checked={isMarried === true}
                                        
                                            // onChange={
                                            //     (e) => {
                                            //         setIsMarried(e.target.checked);
                                            //         console.log(e.target.checked);
                                            //     }
                                                
                                            // }
                                            ></input>
                                            { meta.touched && meta.error?(
                                            <div style={{color:"red"}}>{meta.error}</div>):null
                                            }
                                 </div>
                              )
                            }
                          }
                        </Field>
      </div>
    )
  }
  
  export default FormikCheckBox



         {/* <label htmlFor="isMarried">Are you Married?</label> */}
         {/* <label htmlFor={name}>
                {label} {" "}
                {required?<span style={{color:"red"}}>*</span>:null}
                </label><br></br>
                <input 
                {...Field}
                {...props}
                    type="checkbox" 
                    id={name}
                    onChange={onChange=!!onChange?onChange:field.onChange}
                    checked={meta.value}
                    // value={isMarried}
                    // checked={isMarried === true}
                   
                    // onChange={
                    //     (e) => {
                    //         setIsMarried(e.target.checked);
                    //         console.log(e.target.checked);
                    //     }
                        
                    // }
                    ></input>
                     { meta.touched && meta.error?(
                       <div style={{color:"red"}}>{meta.error}</div>):null
                    } */}
   