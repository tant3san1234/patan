import { logRoles } from '@testing-library/react'
import { Field, Form, Formik } from 'formik'
import React from 'react'
import * as yup from 'yup'
import FormikInput from './FormikInput'
import FormikTextArea from './FormikTextArea'
import FormikSelect from './FormikSelect'
import FormikRadio from './FormikRadio'
import FormikCheckBox from './FormikCheckBox'




const FormikForm = () => {
  //Each Meta has three thing
  //value
  //error
  //touche (field has been touched or not)
  let initialValues={
    firstName:"",
    lastName:"",
    description:"",
    country:"",
    gender:"",
    isMarried:"false"
  }

  let countryOption=[
    {
      label:"select Country",
      value:"",
      disabled: true
    },
    {
      label:"Nepal",
      value:"nep"
    },
    {
      label:"India",
      value:"ind"
    },
    {
      label:"China",
      value:"cin"
    },
    {
      label:"Japan",
      value:"jap"
    },
    {
      label:"Korea",
      value:"kor"
    },
    {
      label:"Bhutan",
      value:"but"
    },

  ]

  let genderOption=[
    {
      label:"Male",
      value:"male",
    },
    {
      label:"Female",
      value:"female",
    },
    {
      label:"Other",
      value:"other",
    },
  ]


  let onSubmit=(value,other)=>{
    console.log(value)
    }

    // validation will run only if
    //onChange event is fired
    //onBlur(touched) event is fired
    //onSubmit event is fired

    let validationSchema = yup.object({
      firstName:yup.string().required("First Name is required"),
      lastName:yup.string().required("Last Name is required"),
      description:yup.string().required("Description is required")
    })
  return (
    <div>
        <Formik
              initialValues={initialValues}
              validationSchema={validationSchema}
              onSubmit={onSubmit}
        >
           {
            (formik)=>{
              /* 
              formik={
                setFieldValue
              } */
                return (
                    <Form>

                      <FormikInput name="firstName"
                       label="First Name"
                       type="text"
                       onChange={(e) => {
                        formik.setFieldValue("firstName",e.target.value)
                      }}
                      placeholder = "First Name"
                      style={{backgroundColor:"darkblue"}}
                      required={true}
                      ></FormikInput> 

                      <FormikInput name="lastName"
                       label="Last Name"
                       type="text"
                       onChange={(e) => {
                        formik.setFieldValue("lastName",e.target.value)
                      }}
                      placeholder = "Last Name"
                      style={{backgroundColor:"darkblue"}}
                      required={true}
                      ></FormikInput> 

                      <FormikTextArea name="description"
                       label="Description"
                       type="text"
                       onChange={(e) => {
                        formik.setFieldValue("description",e.target.value)
                      }}
                      placeholder = "Description"
                      style={{backgroundColor:"darkblue"}}
                      required={true}
                      ></FormikTextArea> 

                      <FormikSelect name="country"
                       label="Country"
                       onChange={(e) => {
                        formik.setFieldValue("country",e.target.value)
                      }}
                      placeholder = "Country"
                      style={{backgroundColor:"darkblue"}}
                      required={true}
                      options={countryOption}
                      ></FormikSelect> 

                      <FormikRadio name="gender"
                       label="Gender"
                       onChange={(e) => {
                        formik.setFieldValue("gender",e.target.value)
                      }}
                      placeholder = "Gender"
                      style={{backgroundColor:"darkblue"}}
                      required={true}
                      options={genderOption}
                      ></FormikRadio> 

                      <FormikCheckBox name="isMarried"
                       label=" Are youMarried"
                       onChange={(e) => {
                        formik.setFieldValue("isMarried",e.target.checked)
                      }}
                      // placeholder = "isMarried"
                      // style={{backgroundColor:"darkblue"}}
                      ></FormikCheckBox> 


                    <button 
                    style={{cursor:"pointer"}}
                    type='submit'
                    >Submit</button>

                    </Form>
                )
            }
           }
        </Formik>
    </div>
  )
}

export default FormikForm