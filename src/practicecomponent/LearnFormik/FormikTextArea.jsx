import { Field } from 'formik'
import React from 'react'

const FormikTextArea = ({name,label,type,onChange,required,...props}) => {

  return (
    <div>
        <Field name ={name}>
                        {
                         ({field,form,meta})=>{
                          // console.log(meta)
                        //  console.log(field)
                        
                        onChange=!!onChange?onChange:field.onChange

                            return(
                              <div>
                               <label htmlFor={name}>{label} 
                               {required?<span style={{color:"red"}}>*</span>:null}
                               </label>
                                  <textarea 
                                  {...field}
                                  {...props}
                                  type={type}
                                   id={name}
                                    value={meta.value}
                                    onChange={onChange}
                                    // onChange={field.onChange} // ==> formik.setFieldValue("firstName",e.target.value)
                                //    onChange={(e) => {
                                //       // console.log("changed");
                                //       // setFirstName(e.target.value);
                                //       formik.setFieldValue("firstName",e.target.value) 
                                //       formik.setFieldValue("lastName",brown) //for putting more than one value put this instead of onChange={field.onChange} 
                                //       // console.log(e.target.value);
                                //   }}
                                  
                                  ></textarea>
                                  { meta.touched && meta.error?(
                                       <div style={{color:"red"}}>{meta.error}</div>):null
                                  }
                              </div>
                            )
                          }
                        }
                      </Field>
    </div>
  )
}

export default FormikTextArea