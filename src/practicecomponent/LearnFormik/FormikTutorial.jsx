import { Formik } from 'formik'
import React from 'react'
import { Form } from 'react-router-dom'
import * as yup from 'yup'

const FormikTutorial = () => {

    let initialValues={
        fullName:"",
        email:"",
        password:"",
        gender:"",
        country:"",
        isMarried:"false",
        description:"",
        phoneNumber:0,
        age:0
    }
   

      let onSubmit=(value,other)=>{
        console.log(value)
        }

        let validationSchema = yup.object({
            fullName:yup.string().required("Full Name is required"),
            email:yup.string().required("email is required"),
            password:yup.string().required("password is required"),
            gender:yup.string().required("gender is required"),
            country:yup.string().required("country is required"),
            isMarried:yup.boolean(),
            description:yup.string(),
            phoneNumber:yup.number().required("phoneNumber is required"),
            age:yup.number().required("age is required")
          })
  return (
    <div>
    <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={onSubmit}
    >
       {
        (formik)=>{
          /* 
          formik={
            setFieldValue
          } */
            return (
                <Form>

                  


                <button 
                style={{cursor:"pointer"}}
                type='submit'
                >Submit</button>

                </Form>
            )
        }
       }
    </Formik>
</div>
  )
}

export default FormikTutorial