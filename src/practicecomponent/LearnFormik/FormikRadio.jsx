import { Field } from 'formik'
import React from 'react'

const FormikRadio = ({name,label,onChange,required,options,...props}) => {

  return (
    <div>
        <Field name ={name}>
                        {
                         ({field,form,meta})=>{
                          // console.log(meta)
                        //  console.log(field)
                        
                            return(
                             <div>
                                <label htmlFor={name}>
                                {label} {" "}
                               {required?<span style={{color:"red"}}>*</span>:null}
                               </label><br></br>

                                {options.map((gen, i) => {
                                return (
                                <div key={i}>
                                        <label htmlFor={gen.value}>{gen.label}</label>
                                        <input
                                        {...field}
                                        {...props}
                                         type="radio" 
                                         id={gen.value} 
                                         value = {gen.value}
                                         onChange={onChange=!!onChange?onChange:field.onChange}
                                         checked={meta.value === gen.value}
                                        //  value={gen.value} 
                                        //  checked={gender === gen.value}
                                        //   onChange={(e) => {
                                        //     setGender(e.target.value);
                                        //     console.log(e.target.value);
                                        // }}
                                        ></input>
                                        { meta.touched && meta.error?(
                                       <div style={{color:"red"}}>{meta.error}</div>):null
                                  }
                                </div>)

                                })}
                             </div>
                            )
                          }
                        }
                      </Field>
    </div>
  )
}

export default FormikRadio