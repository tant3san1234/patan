import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const UpdateSpecificProducts = () => {
  let [name, setName] = useState("");
  let [price, setPrice] = useState("");
  let [quantity, setQuantity] = useState("");
  let params = useParams()
  console.log("params",params)

  let getProduct =async ()=>{
    let result = await axios({
        url:`http://localhost:8000/products/${params.id}`,
        method:"GET"
    })
    console.log("result",result)
    let data= result.data.result
    setName(data.name)
    setPrice(data.price)
    setQuantity(data.quantity)
    // console.log(result)
    // setProducts(result.data.result)
   }
   
   useEffect(()=>{
   getProduct()
      },[])
  let onSubmit = async (e) => {
    e.preventDefault()
   

   
let data = {
      name : name,
      price: price,
      quantity: quantity
    };
    console.log(data);

    try {
      let result = await axios({
        url: `http://localhost:8000/products/${params.id}`,
        method: "PATCH",
        data: data
      });
      console.log(result);
      setName("");
      setPrice("");
      setQuantity("");
      // toast("Product created successfully");
      toast.success(`result.data.message eeeeeeeeeeeeeeeeeeeeeeeeeee`, {
        position: "bottom-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
        });
    } 
    catch (error) {
      toast.error(result.data.result, {
        position: "top-left",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
        });
      }
  }

  return (
    <div>
      {/* <ToastContainer position="bottom-right"/> */}
      <ToastContainer
/>
      <form onSubmit={onSubmit}>
        <label htmlFor="name">Name:</label>
        <input 
          type="text" 
          id="name" 
          placeholder="name" 
          value={name} 
          onChange={
            (e) => {
              setName(e.target.value)
              }
            }></input>
        <br></br>
        <label htmlFor="price">Price: </label>
        <input 
          type="number" 
          id="price" 
          placeholder="price" 
          value={price} 
          onChange={
            (e) => {
              setPrice(e.target.value)
              }
            }></input>
        <br></br>
        <label htmlFor="quantity">Quantity: </label>
        <input 
          type="number" 
          id="quantity" 
          placeholder="quantity" 
          value={quantity} 
          onChange={
            (e) => {
              setQuantity(e.target.value)
            }
          }></input><br></br>
        <button type="submit">Update</button>
      </form>
    </div>
  )
}

export default UpdateSpecificProducts