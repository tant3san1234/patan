import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'

const ReadAllProduct = () => {
    let [products,setProducts] = useState([])
    let navigate = useNavigate()
    // let params = useParams()
  //   { name: "P1", price: 111, quantity: 222 },
  //   { name: "P2", price: 111, quantity: 222 },
  //   { name: "P2", price: 111, quantity: 222 },
  // ])

   let getAllProduct =async ()=>{
    let result = await axios({
        url:`http://localhost:8000/products`,
        method:"GET"
    })
    console.log(result)
    setProducts(result.data.result)
   }
   useEffect(()=>{
   getAllProduct()
      },[])
   
  let deleteProduct=(product)=>{

    return (  async ()=>{
    
      try {
        let result = await axios({
        
          url:`http://localhost:8000/products/${product._id}`,
          method:"DELETE"
      })
      getAllProduct()
      } catch (error) {
        console.log(error.message)
        
      }
    })

   
} 



    let productInfo =  products.map((item,i)=>{
    return(<div key={i} style={{border:"solid darkblue", marginBottom:"10px"}}>
    <p>the product name is {item.name}</p><br></br>
    <p>the product price is NRs {item.price}</p><br></br>
    <p>the product quantity is {item.quantity}</p><br></br>


    <button style={{marginRight:"20px"}} 
    onClick={()=>{
      navigate(`/products/${item._id}`)
    }}
      
    >view</button>


    <button style={{marginRight:"20px"}}
  onClick={(e)=>{
    navigate(`/products/update/${item._id}`)
  }}
    >update</button>
    <button style={{marginRight:"20px"}}
    onClick={deleteProduct(item)}
       
    
    
    >delete</button>
    </div>)
   })



  return (
    <div>
     {productInfo}
    </div>
  )
}

export default ReadAllProduct