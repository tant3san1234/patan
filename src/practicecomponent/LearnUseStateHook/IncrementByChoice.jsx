import React, { useState } from 'react'

const IncrementByChoice = () => {
    let [count,setCount]=useState(0)
    
    let handleClick=(IncrementBy)=>{
        return(e)=>{
            if (IncrementBy==2){
            setCount(count+2)}
            else if (IncrementBy==100){
                setCount(count+100)
            }
            else if (IncrementBy==1000){
                setCount(count+1000)
            }
        }
    }
    

  return (
    <div>My count is {count}<br></br>
    <button onClick={handleClick(2)}>IncrementBy 2</button><br></br>
    <button onClick={handleClick(100)}>IncrementBy 100</button><br></br>
    <button onClick={handleClick(1000)}>IncrementBy 1000</button><br></br>

    </div> 

  )
}

export default IncrementByChoice