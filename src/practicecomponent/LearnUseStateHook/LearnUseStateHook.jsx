import React, { useState } from 'react'

//define variable using usestate
//call variable 
//change variable
const LearnUseStateHook = () => {
    let [name,setName]=useState("ssan")
    let [age,setAge]=useState(99)
    let handleClick=(e)=>{
        setName("dan")
    }
    let handleAge=(e)=>{
        setAge(88)
    }

  return (
    <div>
        my name is {name}

        <br></br>
        <button onClick={handleClick}>Change Name</button>
        <br></br>
         my age is {age}
        <br></br>
        <button onClick={handleAge}>Change Age</button>
    </div>
  )
}

export default LearnUseStateHook