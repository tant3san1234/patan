import React, { useState } from 'react'

const Toggle = () => {
    let [showImg,setShowImg]=useState(true )
    
    let handleImg=(e)=>{
        if(showImg==true){
          setShowImg(false)
        }
        else{
            setShowImg(true)
        }
    }
  return (
    <div>
        {showImg?<img src='./logo512.png'></img>:null}<br></br>
        <button onClick={handleImg}>
            {showImg ===true?"eye close":"eye open"}
             </button>
    </div>
  )
}

export default Toggle