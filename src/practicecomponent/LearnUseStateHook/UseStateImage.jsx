import React, { useState } from 'react'

const UseStateImage = () => {
    let [showImg,setShowImg]=useState(false )
    // let showimgfun =()=>{
    //     setShowImg(true)
    // }
    // let hideimgfun =()=>{
    //     setShowImg(false)
    // }
    let handleImg=(isVisible)=>{
        return(e)=>{
            setShowImg(isVisible)
        }
    }
  return (
    <div>
        {showImg?<img src='./logo512.png'></img>:null}<br></br>
        <button onClick={handleImg(true)}>show</button><br></br>
        <button onClick={handleImg(false)}>hide</button>
    </div>
  )
}
//handleimg  =>     (e)=>{}                    if you dont need to pass value
//handleimg() =>    ()=>{ return ((e)=>{})}    if you need to pass value


export default UseStateImage