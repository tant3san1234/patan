import React, { useState } from 'react'

const LearnState2 = () => {
    let [count,setCount]=useState(0)
    
    let handlecountinc=(e)=>{
        if (count<10){
        setCount(count+1)
            
        }
    }
    let handlecountdec=(e)=>{
        if(count>0){
        setCount(count-1)
        }
    }
    let handlecountres=(e)=>{
        setCount(0)
    }

  return (
    <div>
        my count is {count}<br></br>
        <button onClick={handlecountinc}>Increment</button>
        <button onClick={handlecountdec}>Decrement</button>
        <button onClick={handlecountres}>Reset</button>
    </div>
  )
}

export default LearnState2