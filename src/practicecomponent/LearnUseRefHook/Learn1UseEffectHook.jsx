import React, { useRef } from 'react'

const Learn1UseEffectHook = () => {
    let ref1 = useRef()
    let ref2 = useRef()
    let refInput1 = useRef()
    let refInput2 = useRef()
  return (
    <div>

    <button 
    onClick={(e)=>{
    ref1.current.style.backgroundColor="red"
    }}
    >Change babu</button>


    <button
    onClick={(e)=>{
        ref2.current.style.backgroundColor="blue"
    }}
    >change to blue</button>


    <div
    onClick={(e)=>{
        refInput1.current.focus()
    }}
    >Focus Input</div>

    <div
    onClick={(e)=>{
        refInput1.current.blur()
    }}
    >blur Input1</div>



        <div ref={ref1}>babu</div>
        <div ref={ref2}>nami</div>

        <input ref={refInput1}></input>
        <input ref={refInput2}></input>
    </div>
    
  )
}

export default Learn1UseEffectHook