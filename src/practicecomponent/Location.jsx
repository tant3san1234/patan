import React from 'react'

const Location = ({country, province,district,exactlocation}) => {
  return (
    <div>
      country is {country}
      province is {province}
      district is {district}
      exactlocation is {exactlocation}
    </div>
  )
}

export default Location