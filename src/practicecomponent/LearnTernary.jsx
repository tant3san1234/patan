import React from 'react'

const LearnTernary = () => {
    let age = 18
  return (
    <div>
        {
            age<18?<div>Underage</div>
            :age>=18&&age<=60?<div>Adult</div>
            :<div>Old</div>
        }
    </div>
  )
}

export default LearnTernary