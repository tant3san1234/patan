import axios from 'axios';
import React, { useState } from 'react'
import { ToastContainer, toast } from 'react-toastify';

const AdminRegister = () => {
    let [name, setName] = useState("");
    // let [surname, setSurname] = useState("");
    let [email, setEmail] = useState("");
    let [password, setPassword] = useState("");
    // let [phone, setPhone] = useState("");
    let [dob, setDob] = useState("");
    let [gender, setGender] = useState("male");

    // let [description, setDescription] = useState("");

    let genders = [
        { label: "Male", value: "male" },
        { label: "Female", value: "female" },
        { label: "Other", value: "other" },
      ];
    let onSubmit = async (e) => {
        e.preventDefault();
        let data = {
          fullName: name,
            email: email,
            password: password,
            dob: dob,
            gender:gender,
        };
        console.log(data);
        data = {
            ...data,
            role:"admin"
        }
        // let [year, day, month] = dob.split("-");
        // console.log(`Month = ${month}\nDay = ${day}\nYear = ${year}`);

        try {
            let result = await axios({
                url : "http://localhost:8000/web-users",
                method:"POST",
                data:data
            })
            toast.success("A Link has been sent to your mail,please click link to verify")
            setName("")
            setEmail("")
            setPassword("")
            setDob("")
            setGender("male")
            
        } catch (error) {
            // toast.error(error.response.data.message)
            console.log("error")
        }
    };
  return (
    <div>
        <form onSubmit={onSubmit}>
            <ToastContainer></ToastContainer>
            <div>
                <label htmlFor="name">Name: </label>
                <input type="text" placeholder="Full Name" id="name" value={name} onChange={(e) => {
                    // console.log("changed");
                    setName(e.target.value);
                    // console.log(e.target.value);
                }}></input>
            </div>
            {/* in input whatever you place in value is displayed in the input in the browser */}
        
            {/*email*/}
            <div>
                <label htmlFor="email">email: </label>
                <input 
                type="email" 
                placeholder="example@gmail.com" 
                id="email" 
                value={email} 
                onChange={(e) => {
                    setEmail(e.target.value);
                    // console.log(e.target.value);
                }}></input>
            </div>

            {/*password*/}
            <div>
                <label htmlFor="password">Password: </label>
                <input type="password" 
                id="password" 
                placeholder="your password*" 
                value={password} 
                onChange={(e) => {
                    setPassword(e.target.value);
                    // console.log(e.target.value);
                }}></input>
            </div>

            {/* date of birth  */}
            <div>
                <label htmlFor="dob">Date of Birth</label>
                <input id="dob" type="date" value={dob} onChange={
                    (e) => {
                        setDob(e.target.value);
                        // console.log(e.target.value);
                }}></input>
            </div>

            {/* gender  */}
            <div>
          <label>Gender</label>
          <br />
          {genders.map((item, i) => {
            return (
              <>
                <label htmlFor={item.value}>{item.label}</label>
                <input
                  type="radio"
                  value={item.value}
                  id={item.value}
                  checked={gender === item.value}
                  onChange={(e) => {
                    setGender(e.target.value);
                  }}
                ></input>
              </>
            );
          })}
        </div>

            <br></br>
            <button type="submit">Proceed</button>
        </form>
    </div>
  )
}

export default AdminRegister

//phone number => number
//dob => date