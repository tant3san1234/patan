import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';

const AdminForgotPassword = () => {
    let [email, setEmail] = useState("");
    let navigate = useNavigate()
    let token = localStorage.getItem("token")


    let onSubmit = async (e) => {
        e.preventDefault();
        let data = {
            email: email,
        };
        // console.log(data);
       
        // let [year, day, month] = email.split("-");
        // console.log(`Month = ${month}\nDay = ${day}\nYear = ${year}`);

        try {
            let result = await axios({
                url : "http://localhost:8000/web-users/forgot-password",
                method:"POST",
                data:data,
                
            })

            setEmail("")
            toast.success("Link has been sent")
        //    localStorage.removeItem("token")//this will clear token and 
            // navigate("/admin/login")//go to login
            // navigate("/admin/logout") //this will go from direct logout to homepage clearing token
        } catch (error) {
             toast.error(error.response.data.message)
            console.log("error")
        }
    };

  return (
    <div>
        <form onSubmit={onSubmit}>
            <ToastContainer></ToastContainer>
            
            {/* in input whatever you place in value is displayed in the input in the browser */}
      

            {/* date of birth  */}
            <div>
                <label htmlFor="email">Email</label>
                <input id="email" type="email" value={email} onChange={
                    (e) => {
                        setEmail(e.target.value);
                        // console.log(e.target.value);
                }}></input>
            </div>


            <br></br>
            <button type="submit">Forgot Password</button>
        </form>
    </div>
  )
}

export default AdminForgotPassword

//phone number => number
//email => date