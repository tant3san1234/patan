// import axios from 'axios';
// import React, { useState } from 'react'
// import { useNavigate } from 'react-router-dom';
// import { ToastContainer, toast } from 'react-toastify';

// const AdminLogin = () => {
//     let [email, setEmail] = useState("");
//     let [password, setPassword] = useState("");
//     let navigate = useNavigate

//     let onSubmit = async (e) => {
//         e.preventDefault();
//         let data = {
//             email: email,
//             password: password,
//         };
//         console.log(data);
//         data = {
//             ...data,
//             role:"admin"
//         }
//         // let [year, day, month] = dob.split("-");
//         // console.log(`Month = ${month}\nDay = ${day}\nYear = ${year}`);

//         try {
//             let result = await axios({
//                 url : `http://localhost:8000/web-users`,
//                 method:"POST",
//                 data:data
//             })
//            let token = result.data.token
//            localStorage.setItem("token",token)
//            Navigate("/admin")
            
//         } catch (error) {
//             // toast.error(error.response.data.message)
//             console.log("error")
//         }
//     };
//   return (
//     <div>
//         <form onSubmit={onSubmit}>
//             <ToastContainer></ToastContainer>
            
//             {/* in input whatever you place in value is displayed in the input in the browser */}
        
//             {/*email*/}
//             <div>
//                 <label htmlFor="email">email: </label>
//                 <input 
//                 type="email" 
//                 placeholder="example@gmail.com" 
//                 id="email" 
//                 value={email} 
//                 onChange={(e) => {
//                     setEmail(e.target.value);
//                     console.log(e.target.value);
//                 }}></input>
//             </div>

//             {/*password*/}
//             <div>
//                 <label htmlFor="password">Password: </label>
//                 <input type="password" id="password" placeholder="your password*" value={password} onChange={(e) => {
//                     setPassword(e.target.value);
//                     console.log(e.target.value);
//                 }}></input>
//             </div>

           

//             <br></br>
//             <button type="submit">Login</button>
//         </form>
//     </div>
//   )
// }

// export default AdminLogin

// //phone number => number
// //dob => date




import React, { useState } from "react";

import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import { useNavigate } from "react-router-dom";

const AdminLogin = () => {
 let navigate= useNavigate()
  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");

  let onSubmit = async (e) => {
    e.preventDefault();
    let data = {
     
      email: email,
      password: password,
     
    };

  
    try{
      let result = await axios({
        url:`http://localhost:8000/web-users/login`,
        method:"POST",
        data:data,

      })
     let token= result.data.token;
     localStorage.setItem("token",token)
     navigate("/admin")
     console.log(token)
     
   
    }
    catch(error){
      console.log(error)
    }
    console.log(data);
    // let [year, day, month] = dob.split("-");
    // console.log(`Month = ${month}\nDay = ${day}\nYear = ${year}`);
  };
  
  return (
    <> 
      <ToastContainer></ToastContainer>
      <form onSubmit={onSubmit}>
        
       
        <div>
          <label htmlFor="email">email: </label>
          <input
            type="email"
            placeholder="abc@gmail.com"
            id="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
              console.log(e.target.value);
            }}
          ></input>
        </div>
        {/*password*/}
        <div>
          <label htmlFor="password">Password: </label>
          <input
            type="password"
            id="password"
            placeholder="your password*"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
              console.log(e.target.value);
            }}
          ></input>
        </div>

      
        <br></br>
        <button
        style={{cursor:"pointer"}}

         type="submit">Login</button>
        <div
        style={{cursor:"pointer"}}
        onClick={()=>{
          navigate("/admin/forgot-password")
        }}
        >
          Forgot Password
        </div>
      </form>
    </>
  );
};

export default AdminLogin;