import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Navigate, useNavigate, useParams } from 'react-router-dom'

const AdminReadSpecificUser = () => {
  let token = localStorage.getItem("token")
  

  let [user,setuser]=useState({})
  let navigate = useNavigate()
  let params = useParams()
  let id = params.id

  let getAdminUser = async ()=>{
    try {
      
      let result = await axios({
        url:`http://localhost:8000/web-users/${id}`,
        method:"GET",
        headers:{
          Authorization: `Bearer ${token}`
        }
      })
      console.log(result)
      setuser(result.data.result)
    } 
    catch (error) {
      
    }
  }

  useEffect(()=>{
    getAdminUser()
  },[])


  return (
    <div>
       <p>Full Name ={user.fullName}</p>
       <p>Gender ={user.gender}</p>
       {/* <p>Date of Birth={user.dob}</p> */}
       <p>Date of Birth={new Date(user.dob).toLocaleDateString()}</p>
       {/* <p>Full Name ={user.fullName}</p> */}
       <p> email = {user.email}</p>
       <p> role = {user.role}</p>

       <button
       onClick={()=>{
        navigate("/admin/update-user")
       }}
       >Update User</button>
      
    </div>
  )
}

export default AdminReadSpecificUser