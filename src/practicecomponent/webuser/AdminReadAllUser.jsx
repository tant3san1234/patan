import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'

const AdminReadAllUser = () => {
    let [users,setUsers] = useState([])
    let navigate = useNavigate()
    // let params = useParams()
  //   { name: "P1", price: 111, quantity: 222 },
  //   { name: "P2", price: 111, quantity: 222 },
  //   { name: "P2", price: 111, quantity: 222 },
  // ])

   let getAllUser =async ()=>{
    let result = await axios({
        url:`http://localhost:8000/web-users`,
        method:"GET",
        headers:{
            Authorization:`Bearer ${localStorage.getItem("token")}`
        }

    })
    console.log(result)
    setUsers(result.data.result)
   }
   useEffect(()=>{
   getAllUser()
      },[])
   
  let deleteUser=(user)=>{

    return (  async ()=>{
    
      try {
        let result = await axios({
        
          url:`http://localhost:8000/web-users/${user._id}`,
          method:"DELETE",
          headers:{
            Authorization:`Bearer ${localStorage.getItem("token")}`
        }
      })
      getAllUser()
      } catch (error) {
        console.log(error.message)
        
      }
    })

   
} 



    let userInfo =  users.map((item,i)=>{
    return(<div key={i} style={{border:"solid red", marginBottom:"1px", borderWidth:"500px"}}>
    <p>Full Name  is : {item.fullName}</p><br></br>
    <p>Email is : {item.email}</p><br></br>
    <p>Date of Birth  is : {new Date(item.dob).toLocaleDateString()}</p><br></br>
    <p>Gender is : {item.gender}</p><br></br>
    <p>isVerifiedEmail : {String(item.isVerifiedEmail)}</p><br></br>
    <p>Role is : {item.role}</p><br></br>
    <p>Verified Date is is : {new Date(item.updatedAt).toLocaleDateString()}</p><br></br>


    <button style={{marginRight:"20px"}} 
    onClick={()=>{
      navigate(`/admin/${item._id}`)
    }}
      
    >view</button>


    <button style={{marginRight:"20px"}}
  onClick={(e)=>{
    navigate(`/admin/update/${item._id}`)
  }}
    >update</button>
    <button style={{marginRight:"20px"}}
    onClick={deleteUser(item)}
       
    
    
    >delete</button>
    </div>)
   })



  return (
    <div>
     {userInfo}
    </div>
  )
}

export default AdminReadAllUser