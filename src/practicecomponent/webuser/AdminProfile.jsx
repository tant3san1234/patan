import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Navigate, useNavigate } from 'react-router-dom'

const AdminProfile = () => {
  let token = localStorage.getItem("token")
  

  let [profile,setprofile]=useState({})
  let navigate = useNavigate()

  let getAdminProfile = async ()=>{
    try {
      
      let result = await axios({
        url:"http://localhost:8000/web-users/my-profile",
        method:"GET",
        headers:{
          Authorization: `Bearer ${token}`
        }
      })
      console.log(result)
      setprofile(result.data.result)
    } 
    catch (error) {
      
    }
  }

  useEffect(()=>{
    getAdminProfile()
  },[])


  return (
    <div>
       <p>Full Name ={profile.fullName}</p>
       <p>Gender ={profile.gender}</p>
       {/* <p>Date of Birth={profile.dob}</p> */}
       <p>Date of Birth={new Date(profile.dob).toLocaleDateString()}</p>
       {/* <p>Full Name ={profile.fullName}</p> */}
       <p> email = {profile.email}</p>
       <p> role = {profile.role}</p>

       <button
       onClick={()=>{
        navigate("/admin/update-profile")
       }}
       >Update Profile</button>
      
    </div>
  )
}

export default AdminProfile