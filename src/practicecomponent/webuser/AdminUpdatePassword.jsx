import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';

const AdminUpdatePassword = () => {
    let [oldPassword, setOldPassword] = useState("");
    let [newPassword, setNewPassword] = useState("");
    let navigate = useNavigate()
    let token = localStorage.getItem("token")


    let onSubmit = async (e) => {
        e.preventDefault();
        let data = {
          oldPassword: oldPassword,
            newPassword: newPassword,
        };
        // console.log(data);
       
        // let [year, day, month] = newPassword.split("-");
        // console.log(`Month = ${month}\nDay = ${day}\nYear = ${year}`);

        try {
            let result = await axios({
                url : "http://localhost:8000/web-users/update-password",
                method:"PATCH",
                data:data,
                headers:{
                  Authorization:`bearer ${token}`
                }
            })
           localStorage.removeItem("token")//this will clear token and 
            navigate("/admin/login")//go to login
            // navigate("/admin/logout") //this will go from direct logout to homepage clearing token
        } catch (error) {
             toast.error(error.response.data.message)
            console.log("error")
        }
    };

  return (
    <div>
        <form onSubmit={onSubmit}>
            <ToastContainer></ToastContainer>
            <div>
                <label htmlFor="oldPassword">Old Password </label>
                <input type="Password" id="oldPassword" value={oldPassword} onChange={(e) => {
                    // console.log("changed");
                    setOldPassword(e.target.value);
                    // console.log(e.target.value);
                }}></input>
            </div>
            {/* in input whatever you place in value is displayed in the input in the browser */}
      

            {/* date of birth  */}
            <div>
                <label htmlFor="newPassword">New Password</label>
                <input id="newPassword" type="Password" value={newPassword} onChange={
                    (e) => {
                        setNewPassword(e.target.value);
                        // console.log(e.target.value);
                }}></input>
            </div>


            <br></br>
            <button type="submit">Proceed</button>
        </form>
    </div>
  )
}

export default AdminUpdatePassword

//phone number => number
//newPassword => date