import axios from 'axios'
import React, { useEffect } from 'react'
import { useNavigate, useSearchParams } from 'react-router-dom'

const AdminVerify = () => {
  let navigate=useNavigate()
  let [query]= useSearchParams()
  let token = query.get(`token`)
  let verifyEmail = async()=>{
    try {
      
      let result = await axios({
        url:`http://localhost:8000/web-users/verify-email`,
      method:`PATCH`,
      headers:{
        authorization:`bearer ${token}`
      }
      })

      navigate("/admin/login")
    } catch (error) {
      console.log("error")
    }
  }
  useEffect(()=>{
    verifyEmail()
  })

  return (
    <div>Admin Verified</div>
  )
}

export default AdminVerify