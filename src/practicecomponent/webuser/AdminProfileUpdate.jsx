import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';

const AdminProfileUpdate = () => {
    let [fullName, setFullName] = useState("");
    let [dob, setDob] = useState("");
    let [gender, setGender] = useState("male");
    let navigate = useNavigate()
    let token = localStorage.getItem("token")

    let genders = [
        { label: "Male", value: "male" },
        { label: "Female", value: "female" },
        { label: "Other", value: "other" },
      ];
    let onSubmit = async (e) => {
        e.preventDefault();
        let data = {
          fullName: fullName,
            dob: dob,
            gender:gender,
        };
        // console.log(data);
       
        // let [year, day, month] = dob.split("-");
        // console.log(`Month = ${month}\nDay = ${day}\nYear = ${year}`);

        try {
            let result = await axios({
                url : "http://localhost:8000/web-users/update-profile",
                method:"PATCH",
                data:data,
                headers:{
                  Authorization:`bearer ${token}`
                }
            })
           
            navigate("/admin/my-profile")
        } catch (error) {
             toast.error(error.response.data.message)
            console.log("error")
        }
    };

    let getAdminProfile = async ()=>{
      try {
        
        let result = await axios({
          url:"http://localhost:8000/web-users/my-profile",
          method:"GET",
          headers:{
            Authorization: `Bearer ${token}`
          }
        })
        //  setprofile(result.data.data)
        let data = result.data.result
        console.log(data)
        console.log(data.fullName)
        setDob(data.dob)
        setFullName(data.fullName)
        setGender(data.gender)
      } 
      catch (error) {

        
      }
    }
  
    useEffect(()=>{
      getAdminProfile()
    },[])
  return (
    <div>
        <form onSubmit={onSubmit}>
            <ToastContainer></ToastContainer>
            <div>
                <label htmlFor="fullName">Name: </label>
                <input type="text" placeholder="Full Name" id="fullName" value={fullName} onChange={(e) => {
                    // console.log("changed");
                    setFullName(e.target.value);
                    // console.log(e.target.value);
                }}></input>
            </div>
            {/* in input whatever you place in value is displayed in the input in the browser */}
      

            {/* date of birth  */}
            <div>
                <label htmlFor="dob">Date of Birth</label>
                <input id="dob" type="date" value={dob} onChange={
                    (e) => {
                        setDob(e.target.value);
                        // console.log(e.target.value);
                }}></input>
            </div>

            {/* gender  */}
            <div>
          <label>Gender</label>
          <br />
          {genders.map((item, i) => {
            return (
              <>
                <label htmlFor={item.value}>{item.label}</label>
                <input
                  type="radio"
                  value={item.value}
                  id={item.value}
                  checked={gender === item.value}
                  onChange={(e) => {
                    setGender(e.target.value);
                  }}
                ></input>
              </>
            );
          })}
        </div>

            <br></br>
            <button type="submit">Proceed</button>
        </form>
    </div>
  )
}

export default AdminProfileUpdate

//phone number => number
//dob => date