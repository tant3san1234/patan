import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useNavigate, useSearchParams } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';

const AdminResetPassword = () => {
    let [password, setPassword] = useState("");
    let navigate = useNavigate()

    let [params] = useSearchParams()
    let token = params.get("token")

    let onSubmit = async (e) => {
        e.preventDefault();
        let data = {
            password: password,
        };
        // console.log(data);
       
        // let [year, day, month] = password.split("-");
        // console.log(`Month = ${month}\nDay = ${day}\nYear = ${year}`);

        try {
            let result = await axios({
                url : "http://localhost:8000/web-users/reset-password",
                method:"PATCH",
                data:data,
                headers:{
                  Authorization:`bearer ${token}`
                }
            })
           localStorage.removeItem("token")//this will clear token and 
            navigate("/admin/login")//go to login
            // navigate("/admin/logout") //this will go from direct logout to homepage clearing token
        } catch (error) {
             toast.error(error.response.data.message)
            console.log("error")
        }
    };

  return (
    <div>
        <form onSubmit={onSubmit}>
            <ToastContainer></ToastContainer>
           
            {/* in input whatever you place in value is displayed in the input in the browser */}
      

            {/* date of birth  */}
            <div>
                <label htmlFor="password"> Password</label>
                <input id="password" type="Password" value={password} onChange={
                    (e) => {
                        setPassword(e.target.value);
                        // console.log(e.target.value);
                }}></input>
            </div>


            <br></br>
            <button 
            style={{cursor:"pointer"}}
            type="submit">Reset</button>
        </form>
    </div>
  )
}

export default AdminResetPassword

//phone number => number
//password => date