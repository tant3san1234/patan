import axios from 'axios';
import React, { useState } from 'react'
import { useParams } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const UpdateStudent = () => {
  let [name, setName] = useState("");
  let [age, setAge] = useState("");
  let [isMarried, setIsMarried] = useState(false);
  let onSubmit = async (e) => {
  let params = useParams()
  console.log(`params`,params)
  let getProduct =async ()=>{
    let result = await axios({
        url:`http://localhost:8000/products/${params.id}`,
        method:"GET"
    })
    console.log("result",result)
    let data= result.data.result
    setName(data.name)
    setPrice(data.price)
    setQuantity(data.quantity)
    // console.log(result)
    // setProducts(result.data.result)
   }
   
   useEffect(()=>{
   getProduct()
      },[])
  let onSubmit = async (e) => {
    e.preventDefault()
  
    let data = {
      name : name,
      age: age,
      isMarried: isMarried
    };
    console.log(data);

    try {
      let result = await axios({
        url: `http://localhost:8000/students`,
        method: "POST",
        data: data
      });
      console.log(result);
      setName("");
      setAge("");
      setIsMarried("");
      // toast("student created successfully");
      toast.success(result.data.message, {
        position: "bottom-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
        });
    } 
    catch (error) {
        toast.error('student not created', {
        position: "bottom-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
        });
      }
  }

  return (
    <div>
      {/* <ToastContainer position="bottom-right"/> */}
      <ToastContainer
/>
      <form onSubmit={onSubmit}>
        <label htmlFor="name">Name:</label>
        <input 
          type="text" 
          id="name" 
          placeholder="name" 
          value={name} 
          onChange={
            (e) => {
              setName(e.target.value)
              }
            }></input>
        <br></br>
        <label htmlFor="age">age: </label>
        <input 
          type="number" 
          id="age" 
          placeholder="age" 
          value={age} 
          onChange={
            (e) => {
              setAge(e.target.value)
              }
            }></input>
        <br></br>
        <label htmlFor="isMarried">isMarried: </label>
        <input 
          type="checkbox" 
          // type="boolean" 
          id="isMarried" 
          // placeholder="isMarried" 
            checked={isMarried==true}
          value={isMarried} 
          onChange={
            (e) => {
              // setIsMarried(e.target.value)
              setIsMarried(e.target.checked)
            }
          }></input> <br></br> 
        <button type="submit">Submit</button>
      </form>
    </div>
  )
}
}

export default UpdateStudent