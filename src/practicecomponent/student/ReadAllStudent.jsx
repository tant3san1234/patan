import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'

const ReadAllStudent = () => {

  let [students,setStudents] = useState([])
  let navigate=useNavigate()

  let getAllStudent = async()=>{
    let result = await axios({
      url:`http://localhost:8000/students`,
      method:`GET`
    })
        setStudents(result.data.result)
  }

  useEffect(()=>{
    getAllStudent()
  },[])

    let studentinfo = students.map((item,i)=>{
     return(<div key={i} style={{border:`solid purple`,marginBottom:`20px`}}>
      <p>the student name is {item.name}</p><br></br>
      <p>the student age is {item.age}</p><br></br>
      <p> is the student married ? {item.isMarried?"true":"false"}</p><br></br>


      <button style={{marginRight:"20px"}} 
    onClick={()=>{
      navigate(`/students/${item._id}`)
    }}
      
    >view</button>


    <button style={{marginRight:"20px"}}
    onClick={(e)=>{
      navigate(`/students/update/${item._id}`)
    }}
    >update</button>
    <button style={{marginRight:"20px"}}>delete</button>
     </div>)
    })


  return (
    <div>{studentinfo}</div>
  )
}

export default ReadAllStudent