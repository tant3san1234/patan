import React, { useContext } from 'react'
import { context1, context2 } from '../../App'

const GrandChild = () => {

    let value =useContext(context1)
    let value2 =useContext(context2)
  return (
    <div>GrandChild<br></br>
        the name is  {value.name}<br></br>
        the age is {value.age}<br></br>
        the address is {value2.address}<br></br>
        <button onClick={()=>{value2.setAddress("gagal")}}>change address</button>
        <button onClick={()=>{
            value.setName("grandchild")
        }}
        >change Name</button>
    </div>
  )
}

export default GrandChild