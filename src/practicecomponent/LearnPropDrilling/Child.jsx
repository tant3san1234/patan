import React, { useContext } from 'react'
import GrandChild from './GrandChild'
import { context1, context2 } from '../../App'

const Child = () => {
    let value =useContext(context1)
    let value2 =useContext(context2)

  return (
    <div style={{border:"solid red 3px"}}>Child
      
        the name is  {value.name}<br></br>
        the age is {value.age}<br></br>
        the address is {value2.address}<br></br>
        <button onClick={()=>{value2.setAddress("bala")}}>change address</button>
        <button onClick={()=>{
            value.setName("child")
        }}
        >change Name</button>
          <GrandChild></GrandChild>
   
    </div>
  )
}

export default Child