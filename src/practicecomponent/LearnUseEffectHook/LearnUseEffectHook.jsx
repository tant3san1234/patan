import React, { useEffect, useState } from 'react'

const LearnUseEffectHook = () => {
    let [count,setCount]=useState(0)
    let [count2,setCount2]=useState(100)

    //it executes for 1st render
    //from 2nd render the fun will only render if count and count2 variable is changed 
    useEffect(()=>{
        console.log("this is useEffect function")
    },[count,count2])
    console.log("**")

    // it ececutes for 1st render
    //from second render the function will executes if count variable is changed
    useEffect(()=>{
      console.log("i am useEffect 2")
    },[count])

    //it ececutes for first render only
    useEffect(()=>{
      console.log("i am useEffect 3")
    },[])

    //it executes for each render
    useEffect(()=>{
      console.log("i am useEffect 4")
    })


  return (
    <div> 
        count is {count}<br></br>
        <button 
                onClick={(e)=>{
                    setCount(count+1)
                }}
         >Increament</button><br></br>\count2 is {count}<br></br>
         <button onClick={(e)=>{
          setCount(count+100)
         }}>
          Increament2
         </button>
    </div>
  )
}

export default LearnUseEffectHook