import React, { useEffect, useState } from 'react'

const LearnCleanUpFunction = () => {
    let [count,setCount]=useState(0)
   useEffect(()=>{
           console.log("i am useEffect")
           return()=>{
            
              console.log("i am cleanup function")
            
           }
   },[count])
/*    // cleanup function are function which is return by useEffect
  cleanup function does not execute in first render
  but from second render the cleanup function will ecexute if useEffect func will rin

  what 
  first

  when component is unmound nothing gets executed execpt cleanup function

  lifecycle of component
  component did mound(first render)
     useeffect fun will run
  component did update(2nd render)(when state variable change)
     useeffect fun will only run if its dependency changes
  component did unmount(component removed)
     nothing gets execute
     but cleanup function will execute
*/
  return (
    <div>
        count1 is {count}<br></br>
        <button onClick={()=>{
            setCount(count+1)
        }}>Increament count by 1</button>
    </div>
  )
}

export default LearnCleanUpFunction