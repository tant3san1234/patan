import React from 'react'

const RemoveLocalStorageData = () => {
  return (
    <div>
        <button
        onClick={(e)=>{
            localStorage.removeItem("token")
        }}
        >Remove token</button><br></br>
        <button
        onClick={(e)=>{
            localStorage.removeItem("name")
        }}
        >Remove name</button><br></br>
        <button
        onClick={(e)=>{
            localStorage.removeItem("age")
        }}
        >Remove age</button><br></br>
        <button
        onClick={(e)=>{
            localStorage.removeItem("isMarried")
        }}
        >Remove isMarried</button><br></br>
        <button
        onClick={(e)=>{
            localStorage.removeItem("isMa")
        }}
        >Remove isMa</button><br></br>
    </div>
  )
}

export default RemoveLocalStorageData


// local storage is the browser`s memory for the particular url
//the data in a local storage persists even when the session and (browser close)(tab close)