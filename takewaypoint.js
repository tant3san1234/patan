/*
Day 1
A react variable can store tag
we can implement javascript operation inside tag using {}
there must be only on wrapper
anything that is written inside will display in the browser

block element
 it takes all width
 it always starts from new line
inline element
 it takes required width

image
 always place images or files at public folder
 .at src of image refers to public folder

css
 inline
  obj is used for styling purposes
  you must use cames case convection

 external
   it is a three step process
    define
    import
    use

 in react console will appear at developer pannel
   
*/

/*
day 2

 component
  it is function whose first letter is capital
  call component like html tag
  if props is other than string wrap it by curley braces{}
  component are the custom tag (user defined tag)
  inbuilt props(className , style) are only support in inbult tag(html tag) ie it is not supported in the custom tag
  jsx =>
  it is the extension
   it is the combination of java script and html 
*/

/* 
day 3


Limitation of curley braces
=> it must return only one value
=> it doesnot support if, else, for, while, do-while
   we cannot define variables inside curley braces

limitations of ternary operator
=> it must have else(:) part  

Effect of different data in side html tag
  boolean are not shown in browser for this we have to add some logic
  dont call object inside html tag children


  function of usestate
   a page will render if state variable is changes 

   when a stare variable changes
   a component gets render such that
   the state variable which is changed holds values
   whereas other state variable holds previous value

   function of wseEffect
   it is asynchronous function (executes after all the items is displayed )
   useEffect function only executes in first render


   before working with react router dom you have to wrap app component by browser router

   day 10
For all input types:            value      e.target.value
for checkbox:                   checked    e.target.checked
For Radio-button:               checked    e.target.value
*/

